const { getMongoDatabase } = require("../services/mongodb");

function createMongodbMiddleware(mongoClient) {
  const middleware = async function (req, res, next) {
    const db = getMongoDatabase(mongoClient);

    if (!db) {
      res.status(500).send({ error: "Failed to connect to MongoDB" });
    }

    req.db = db;

    next();
  };
  return middleware;
}

module.exports = { createMongodbMiddleware };
