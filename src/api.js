const cors = require("cors");
const express = require("express");
const { getAllUsers, createUser, deleteUser } = require("./controllers");
const {
  createMongodbMiddleware,
  createAuthenticationMiddleware,
  createStanConnectionMiddleware,
} = require("./middlewares");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */

  // Middlewares
  const mongoMiddleware = createMongodbMiddleware(mongoClient);
  const stanMiddleware = createStanConnectionMiddleware(stanConn);
  const authenticateToken = createAuthenticationMiddleware(secret);

  // Rota para propositos de teste
  api.get("/users", mongoMiddleware, getAllUsers);

  api.post("/users", mongoMiddleware, stanMiddleware, createUser);

  api.delete(
    "/users/:uuid",
    authenticateToken,
    mongoMiddleware,
    stanMiddleware,
    deleteUser
  );

  /* ******************* */

  return api;
};
