const stan = require("node-nats-streaming");
const { getMessageContent } = require("./services/nats");
const {
  getMongoDatabase,
  createUser,
  deleteUser,
} = require("./services/mongodb");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */

    const db = getMongoDatabase(mongoClient);

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const usersSubscription = conn.subscribe("users", opts);
    const actionsByEventType = {
      UserCreated: createUser,
      UserDeleted: deleteUser,
    };

    usersSubscription.on("message", async (msg) => {
      const { eventType, ...data } = getMessageContent(msg);
      const action = actionsByEventType[eventType];
      await action(db, data);
    });

    /* ******************* */
  });

  return conn;
};
