const { v4: uuid } = require("uuid");
const { USERS } = require("./collections");
const { validateFields } = require("./validators");
const { createMessage } = require("./services/nats");

async function getAllUsers(req, res) {
  const response = await req.db.collection(USERS).find({}).toArray();
  res.status(200).json({ users: response });
}

async function createUser(req, res) {
  const { statusCode, errorMessage } = validateFields(req.body);

  if (errorMessage) {
    res.status(statusCode).json({ error: errorMessage });
    return;
  }

  const { name, email, password } = req.body;
  const alreadyExistsUserWithEmail = await req.db
    .collection(USERS)
    .findOne({ email });

  if (!alreadyExistsUserWithEmail) {
    const id = await generateNewId(req.db);
    const message = createMessage("UserCreated", id, { name, email, password });
    req.sc.publish("users", message);
    res.status(201).json({ user: { id, name, email } });
  } else {
    res
      .status(409)
      .json({ error: "There is already an account with this email" });
  }
}

async function deleteUser(req, res) {
  const id = req.params.uuid;

  const message = createMessage("UserDeleted", id);
  req.sc.publish("users", message);

  res.status(200).json({ id });
}

async function generateNewId(db) {
  let id = uuid();
  let user = await db.collection(USERS).findOne({ _id: id });

  while (user) {
    id = uuid();
    user = await db.collection(USERS).findOne({ _id: id });
  }

  return id;
}

module.exports = { getAllUsers, createUser, deleteUser };
