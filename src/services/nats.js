function createMessage(type, id, data) {
  return JSON.stringify({
    eventType: type,
    entityId: id,
    entityAggregate: data || {},
  });
}

function getMessageContent(message) {
  const content = JSON.parse(message.getData());
  return content;
}

module.exports = { createMessage, getMessageContent };
