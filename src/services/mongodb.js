const { USERS } = require("../collections");

function getMongoDatabase(mongoClient) {
  const databaseName = process.env.MONGO_DATABASE_NAME;
  const database = mongoClient.db(databaseName);
  return database;
}

async function createUser(db, data) {
  const { entityId, entityAggregate } = data;
  const newUser = { _id: entityId, ...entityAggregate };
  await db.collection(USERS).insertOne(newUser);
}

async function deleteUser(db, data) {
  const { entityId } = data;
  await db.collection(USERS).deleteOne({ _id: entityId });
}

module.exports = { getMongoDatabase, createUser, deleteUser };
