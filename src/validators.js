const jwt = require("jsonwebtoken");

function validateFields(fields) {
    const fieldsErrorMessage = validateMissingFields(fields) || validateMalformedFields(fields);
    const passwordMatchErrorMessage = validatePasswordsMatch(fields);

    let statusCode = null;
    let errorMessage = null;

    if (fieldsErrorMessage) {
        statusCode = 400;
        errorMessage = fieldsErrorMessage;
    } else if (passwordMatchErrorMessage) {
        statusCode = 422;
        errorMessage = passwordMatchErrorMessage;
    }

    return { statusCode, errorMessage };
}

function validateToken(userId, token, secret) {
    let statusCode = null;
    let errorMessage = null;
    
    if (token) {
        const { id } = jwt.verify(token, secret);        
        if (userId !== id) {
            statusCode = 403;
            errorMessage = "Access Token did not match User ID"
        }
    } else {
        statusCode = 401;
        errorMessage = "Access Token not found";
    }

    return { statusCode, errorMessage };
}

function validateMissingFields({ name, email, password, passwordConfirmation }) {
    let missingFieldName = null;

    if (!name) {
        missingFieldName = "name";
    } else if (!email) {
        missingFieldName = "email";
    } else if (!password) {
        missingFieldName = "password";
    } else if (!passwordConfirmation) {
        missingFieldName = "password confirmation";
    }

    if (missingFieldName) {
        return `Request body had missing field ${missingFieldName}`;
    }

    return null;
}

function validateMalformedFields({ email, password }) {
    let malformedFieldName = null;

    if (!isEmailValid(email)) {
        malformedFieldName = "email";
    } else if (!isPasswordValid(password)) {
        malformedFieldName = "password";
    }

    if (malformedFieldName) {
        return `Request body had malformed field ${malformedFieldName}`;
    }

    return null;
}

function isEmailValid(email) {
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    return emailRegex.test(String(email));
}

function isPasswordValid(password) {
    const passwordRegex = /^[a-zA-Z0-9]{8,32}$/i;
    return passwordRegex.test(String(password));
}

function validatePasswordsMatch({ password, passwordConfirmation }) {
    const errorMessage = `Password confirmation did not match`;
    return password === passwordConfirmation ? null : errorMessage;
}

module.exports = { validateFields, validateToken };